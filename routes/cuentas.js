import cuentaController from '../controllers/cuentasController.js';
import express from 'express';
const router = express.Router();
  
// API / Cuentas
  
// GET - Listar cuentas
router.get('/', cuentaController.listar);

// GET - Mostrar cuenta
router.get('/:id', cuentaController.mostrar);
  
// POST - Añadir cuenta
router.post('/', cuentaController.crear);
  
// PUT - Modificar cuenta
router.put('/:id', cuentaController.editar);
  
// DELETE - Eliminar cuenta
router.delete('/:id', cuentaController.eliminar);
  
export default router;
  