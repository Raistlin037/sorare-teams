import Cuentas from '../models/Cuentas.js';

const cuentaController = {

    listar: async (req, res) => {
        try {
            let cuentas = await Cuentas.find();

            res.json(cuentas);

        } catch (error) {
            console.log(error);
            res.status(500).json({origen: 'Controlador listar cuentas', msg: error});
        }
    },
    mostrar: async (req, res) => {
        try {
            let cuenta = await Cuentas.findById(req.params.id);

            if(!cuenta) res.status(404).json({origen: 'Controlador mostrar cuenta', msg: 'La cuenta no existe'});
            res.json(cuenta);

        } catch (error) {
            console.log(error);
            res.status(500).json({origen: 'Controlador mostrar cuenta', msg: error});
        }
    },
    crear: async (req, res) => {
        try {
            let cuenta = new Cuentas(req.body);

            await cuenta.save();
            res.json(cuenta);

        } catch (error) {
            console.log(error);
            res.status(500).json({origen: 'Controlador crear cuenta', msg: error});
        }
    },
    editar: async (req, res) => {
        try {
            let cuenta = await Cuentas.findById(req.params.id);

            if(!cuenta) res.status(404).json({origen: 'Controlador editar cuenta', msg: 'La cuenta no existe'});

            cuenta = await Cuentas.findOneAndUpdate({ _id: req.params.id }, {nombre: req.body.nombre, gestor: req.body.gestor}, { new: true });
            res.json(cuenta);
            
        } catch (error) {
            console.log(error);
            res.status(500).json({origen: 'Controlador editar cuenta', msg: error});
        }
    },
    eliminar: async (req, res) => {
        try {
            let cuenta = await Cuentas.findById(req.params.id);

            if(!cuenta) res.status(404).json({origen: 'Controlador eliminar cuenta', msg: 'La cuenta no existe'});
            await Cuentas.findOneAndRemove({ _id: req.params.id });
            res.json({origen: 'Controlador eliminar cuenta', msg: 'Cuenta eliminada'});

        } catch (error) {
            console.log(error);
            res.status(500).json({origen: 'Controlador eliminar cuenta', msg: error});
        }
    }
}

export default cuentaController;