import mongoose from 'mongoose';

const CuentasSchema = new mongoose.Schema({
    nombre: {
        type: String,
        required: true
    },
    gestor: {
        type: String,
        required: true
    }
});

const Cuentas = mongoose.model('Cuentas', CuentasSchema);

export default Cuentas;