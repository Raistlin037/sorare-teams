export class Cuenta {
    _id?: number;
    nombre: string;
    gestor: string;

    constructor (_id:number, nombre: string, gestor: string) {
        this._id = _id;
        this.nombre = nombre;
        this.gestor = gestor;
    }
}