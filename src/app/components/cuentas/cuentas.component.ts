import { Component, OnInit } from '@angular/core';
import { CuentaService } from 'src/app/services/cuenta.service';
import { Cuenta } from 'src/app/models/cuenta';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-cuentas',
  templateUrl: './cuentas.component.html',
  styleUrls: ['./cuentas.component.scss']
})
export class CuentasComponent implements OnInit {
  cuentas: Cuenta[] = [];

  constructor(private _cuentaService: CuentaService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.listarCuentas();
  }

  listarCuentas() {
    this._cuentaService.getCuentas().subscribe( data => {
      this.cuentas = data;
    }, error => {
      console.log(error);
    });
  }

  eliminarCuenta(id: any) {
    this._cuentaService.deleteCuenta(id).subscribe( data => {
      let indice = this.cuentas.findIndex(c => c._id == id);
      this.cuentas.splice(indice,1);
      this.toastr.error('Cuenta eliminada');
    }, error => {
      console.log(error);
    });
  }

}
