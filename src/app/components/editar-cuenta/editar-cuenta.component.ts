import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Cuenta } from 'src/app/models/cuenta';
import { CuentaService } from 'src/app/services/cuenta.service';

@Component({
  selector: 'app-editar-cuenta',
  templateUrl: './editar-cuenta.component.html',
  styleUrls: ['./editar-cuenta.component.scss']
})
export class EditarCuentaComponent implements OnInit {
  cuentaForm: FormGroup;
  titulo: string = "Nueva cuenta";
  id: string | null;

  constructor(private _cuentaService: CuentaService, 
              private fb:FormBuilder, 
              private router: Router, 
              private toastr: ToastrService, 
              private aRouter: ActivatedRoute) {
    this.cuentaForm = this.fb.group({
      nombre: ['', Validators.required],
      gestor: ['', Validators.required]
    }),
    this.id = this.aRouter.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.esEditar();
  }
  
  modificarCuenta(){
    const CUENTA: Cuenta = {
      nombre: this.cuentaForm.get('nombre')?.value,
      gestor: this.cuentaForm.get('gestor')?.value,
    }

    if(this.id != null) {
      // edición de la cuenta
      this._cuentaService.editCuenta(this.id, CUENTA).subscribe( data => {
        this.toastr.info('Cuenta modificada');
        this.router.navigate(['/cuentas']);
      }, error => {
        this.cuentaForm.reset();
        this.toastr.error('Cuenta no modificada');
        console.log(error);
      });
    } else {
      // creacción de una cuenta
      this._cuentaService.saveCuenta(CUENTA).subscribe( data => {
        this.toastr.info('Cuenta registrada');
        this.router.navigate(['/cuentas']);
      }, error => {
        this.cuentaForm.reset();
        this.toastr.error('Cuenta no registrada');
        console.log(error);
      });
    }
  }

  esEditar() {
    if(this.id != null) {
      this.titulo = 'Editar cuenta';
      this._cuentaService.getCuenta(this.id).subscribe( data => {
        this.cuentaForm.setValue({ nombre: data.nombre, gestor: data.gestor });
      }, error => {
        console.log(error);
      });
    }
  }
}
