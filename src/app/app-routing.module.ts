import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Componentes
import { CuentasComponent } from './components/cuentas/cuentas.component';
import { EditarCuentaComponent } from './components/editar-cuenta/editar-cuenta.component';

const routes: Routes = [
  { path: 'cuentas', component: CuentasComponent},
  { path: 'cuentas/crear', component:EditarCuentaComponent },
  { path: 'cuentas/editar/:id', component:EditarCuentaComponent },
  { path: '**', redirectTo:'cuentas', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
