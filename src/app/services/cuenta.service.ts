import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Cuenta } from 'src/app/models/cuenta';

@Injectable({
  providedIn: 'root'
})

export class CuentaService {
  url = 'http://localhost:4000/api';

  constructor(private http: HttpClient) { }

  getCuentas(): Observable<any> {
    return this.http.get(`${this.url}/cuentas`);
  }
  
  getCuenta(id: string): Observable<any> {
    return this.http.get(`${this.url}/cuentas/${id}`);
  }

  saveCuenta(cuenta: Cuenta): Observable<any> {
    return this.http.post(`${this.url}/cuentas`, cuenta);
  }

  editCuenta(id: string, cuenta: Cuenta): Observable<any> {
    return this.http.put(`${this.url}/cuentas/${id}`, cuenta);
  }

  deleteCuenta(id: string): Observable<any> {
    return this.http.delete(`${this.url}/cuentas/${id}`);
  }
  
}
