import express from 'express';
import conectarDB from './config/db.js';
import router from './routes/cuentas.js';
import cors from 'cors';

// Creación del servidor
const app = express();
app.use(express.json());
app.use(cors());

// Conectar base de datos
conectarDB();

// Rutas
app.use('/api/cuentas', router);

app.listen(4000, () => {
    console.log('Aplicación conectada')
})